<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

// Add module configuration
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
'module.tx_form {
    settings {
        yamlConfigurations {
            1660751339 = EXT:form_mailchimp/Configuration/Yaml/FormSetup.yaml
        }
    }
}'
);
