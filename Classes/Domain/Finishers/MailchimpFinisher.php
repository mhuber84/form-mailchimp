<?php

namespace Mhuber84\FormMailchimp\Domain\Finishers;

use MailchimpMarketing\ApiClient;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher;
use TYPO3\CMS\Form\Domain\Finishers\Exception\FinisherException;

/**
 * This finisher registers a user at Mailchimp
 *
 * - mailchimpListId (mandatory): Mailchimp List ID (Audience)
 * - email (mandatory): Email addresse to register
 * - newsletter (mandatory): Checkbox if the user wants it
 * - fieldMapping: Array with Mailchimp fieldname to EXT:form fieldname
 * - mailchimpTags: CSV list with mailchimp tags
 *
 * Scope: frontend
 */
class MailchimpFinisher extends AbstractFinisher
{

    /**
     * @var array
     */
    protected $defaultOptions = [
        'mailchimpListId' => '',
        'email' => '',
        'newsletter' => false,
        'fieldMapping' => [],
        'mailchimpTags' => '',
    ];

    /**
     * Executes this finisher
     * @see AbstractFinisher::execute()
     *
     * @throws FinisherException
     */
    protected function executeInternal()
    {

        $mailchimpListId = $this->parseOption('mailchimpListId');
        $email = $this->parseOption('email');
        $newsletter = $this->parseOption('newsletter');
        $mailchimpTags = $this->parseOption('mailchimpTags');
        $mergeFields = $this->parseOption('fieldMapping') ?? [];

        if (empty($mailchimpListId)) {
            throw new FinisherException('The option "Mailchimp List ID" must be set for the MailchimpFinisher.', 1660751339);
        }
        if (empty($email)) {
            throw new FinisherException('The option "email" must be set for the MailchimpFinisher.', 1660751760);
        }

        if($newsletter === '1'){
            $extSettings = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('form_mailchimp');
            $mailchimp = new ApiClient();
            $mailchimp->setConfig([
                'apiKey' => $extSettings['mailchimp']['apiKey'],
                'server' => $extSettings['mailchimp']['serverPrefix']
            ]);

            $subsciberHash = md5(strtolower($email));

            try {
                $response = $mailchimp->lists->getListMember($mailchimpListId, $subsciberHash);
            } catch (\Exception $e){
                $response = false;
            }

            if($response === false || $response->status !== 'subscribed'){
                try {
                    $data = [
                        'email_address' => $email,
                        'status_if_new' => 'pending',
                        'status' => 'pending',
                        'merge_fields' => $mergeFields,
                    ];
                    $tags = GeneralUtility::trimExplode(',', $mailchimpTags, true);
                    if(!empty($tags)) {
                        $data['tags'] = $tags;
                    }
                    $response = $mailchimp->lists->setListMember($mailchimpListId, $subsciberHash, $data);
                } catch (\Exception $e){
                    $response = false;
                }
            }
        }
    }

}
