# TYPO3 Extension "EXT:form to Mailchimp"

This extension adds a finisher for the TYPO3 sysext "form" to register the user to your Mailchimp audience.

When you add the form finisher, you can select the email field and the newsletter field in your form and define the
Mailchimp list id. You can also define tags. If a user submits the form he is added to your Mailchimp audience.

## Installation

1. Install with `composer require mhuber84/form-mailchimp`

2. Include TypoScript
Check if this overwrites your own settings and adjust if necessary! :-)

3. Include finisher in your form

4. Configure API in ExtensionManager

### Example form with mailchimp finisher

    type: Form
    identifier: newsletter_registration
    label: 'Newsletter registration'
    prototypeName: standard
    renderingOptions:
      submitButtonLabel: Send
    finishers:
      -
        options:
          mailchimpListId: 'YOUR-MAILCHIMP-LIST-ID'
          email: '{email}'
          newsletter: '{newsletter}'
          fieldMapping:
            FNAME: '{firstname}'
            LNAME: '{lastname}'
          mailchimpTags: 'Tag1, Tag2'
        identifier: Mailchimp
    renderables:
      -
        renderingOptions:
          previousButtonLabel: 'Previous step'
          nextButtonLabel: 'Next step'
        type: Page
        identifier: page-1
        label: ''
        renderables:
          -
            type: GridRow
            identifier: gridrow-1
            label: 'Grid: Row'
            renderables:
              -
                type: Fieldset
                identifier: fieldset-1
                label: ''
                renderables:
                  -
                    defaultValue: ''
                    type: Text
                    identifier: firstname
                    label: Firstname
                  -
                    defaultValue: ''
                    type: Text
                    identifier: lastname
                    label: Lastname
                  -
                    defaultValue: ''
                    type: Email
                    identifier: email
                    label: E-Mail
                    properties:
                      fluidAdditionalAttributes:
                        required: required
                      validationErrorMessages:
                        -
                          code: 1221560910
                          message: Required
                        -
                          code: 1221560718
                          message: Required
                        -
                          code: 1347992400
                          message: Required
                        -
                          code: 1347992453
                          message: Required
                    validators:
                      -
                        identifier: EmailAddress
                      -
                        identifier: NotEmpty
                  -
                    type: Checkbox
                    identifier: newsletter
                    label: 'Newsletter'

## Author

<a href="https://marco-huber.de">Marco Huber</a>, <a href="https://troet.cafe/@mhuber84">@mhuber84@troet.cafe</a>, <a href="mailto:mail@marco-huber.de">mail@marco-huber.de</a>

Feel free to contribute and send in pull requests or create an issue on <a href="https://gitlab.com/mhuber84/femanager-mailchimp">https://gitlab.com/mhuber84/form-mailchimp</a>
